<?php

declare(strict_types=1);

namespace Grifix\Test;

use Grifix\EventStore\EventStoreInterface;
use Grifix\EventStore\Workers\EventProcessor;
use Grifix\EventStore\Workers\EventPublisher;
use Grifix\Uuid\Uuid;
use Throwable;

final class TestMessageBus
{
    private ?\Throwable $lastError = null;

    public function __construct(
        private readonly EventStoreInterface $eventStore,
        private readonly EventPublisher $eventPublisher,
        private readonly EventProcessor $eventProcessor
    ) {
    }

    public function wasMessageSent(object $event): bool
    {
        foreach ($this->eventStore->findEvents() as $envelope) {
            if ($envelope->event == $event) {
                return true;
            }
        }
        return false;
    }

    public function sendMessage(string $streamType, Uuid $streamId, object $message): void
    {
        try {
            $this->eventStore->storeEvent($message, $streamType, $streamId);
            $this->eventStore->flush();
            $this->eventPublisher->__invoke();
            foreach ($this->eventStore->findEvents() as $event) {
                $this->eventProcessor->__invoke($event);
            }
        } catch (Throwable $exception) {
            $this->lastError = $exception;
        }
    }

    public function getLastError(): ?Throwable
    {
        return $this->lastError;
    }
}
