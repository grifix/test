<?php

declare(strict_types=1);

namespace Grifix\Test;

use Grifix\ArrayWrapper\ArrayWrapper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;

final class TestHttpClient
{
    private ?Response $lastResponse = null;

    public function __construct(private readonly KernelInterface $kernel)
    {
    }

    public function get(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): Response {
        return $this->sendRequest('get', $url, $body, $queryString, $headers);
    }

    public function head(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): Response {
        return $this->sendRequest('head', $url, $body, $queryString, $headers);
    }

    public function delete(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): Response {
        return $this->sendRequest('delete', $url, $body, $queryString, $headers);
    }

    public function options(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): Response {
        return $this->sendRequest('options', $url, $body, $queryString, $headers);
    }

    public function post(
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): Response {
        return $this->sendRequest('post', $url, $body, $queryString, $headers);
    }

    public function put(
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): Response {
        return $this->sendRequest('put', $url, $body, $queryString, $headers);
    }

    public function path(
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): Response {
        return $this->sendRequest('path', $url, $body, $queryString, $headers);
    }

    private function sendRequest(
        string $method,
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): Response {
        $request = Request::create($url, $method, $queryString, content: $body);
        if ($headers) {
            $request->headers->add($headers);
        }
        $this->lastResponse = $this->kernel->handle($request);

        return $this->lastResponse;
    }

    public function getLastResponse(): Response
    {
        return $this->lastResponse;
    }

    public function getLastResponseJson(): ArrayWrapper
    {
        return ArrayWrapper::create(
            json_decode($this->lastResponse->getContent(), true)
        );
    }

    public function getLastResponseStatusCode(): int
    {
        return $this->lastResponse->getStatusCode();
    }

    public function buildPayload(array $payload, array $override = [], array $remove = []): array{
        $result = ArrayWrapper::create($payload);
        foreach ($remove as $pathKey){
            $result->removeElement($pathKey);
        }
        return array_merge($result->getWrapped(), $override);
    }
}
