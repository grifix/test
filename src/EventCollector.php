<?php
declare(strict_types=1);

namespace Grifix\Test;

use Grifix\Reflection\ReflectionObject;
use Mockery\MockInterface;

final class EventCollector
{
    /** @var object[] */
    private array $events = [];

    public function add(object $event): void
    {
        $this->events[] = $event;
    }

    public function clear(): void
    {
        $this->events = [];
    }

    public function get(): array
    {
        return $this->events;
    }

    public static function injectTo(MockInterface $mock, string $method = 'publishEvent'): void
    {
        $eventCollector = new self();
        $mock->allows($method)->andReturnUsing(
            function (object $event) use ($eventCollector): void {
                $eventCollector->add($event);
            }
        );

        $mock->allows('__getEventCollector')->andReturn($eventCollector);
    }

    public static function getEvents(object $object): array
    {
        return self::getEventCollector($object)->get();
    }

    public static function clearEvents(object $object): void
    {
        self::getEventCollector($object)->clear();
    }

    private static function getEventCollector(object $object, string $property = 'outside'): EventCollector
    {
        /** @var EventCollector $result */
        $result = (new ReflectionObject($object))->getPropertyValue($property)->__getEventCollector();
        return $result;
    }
}
