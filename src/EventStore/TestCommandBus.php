<?php

declare(strict_types=1);

namespace Grifix\Test\EventStore;

use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\EventStoreInterface;
use Grifix\EventStore\Workers\EventProcessor;
use Grifix\EventStore\Workers\EventPublisher;
use Grifix\Framework\Application\CommandBusInterface;

final class TestCommandBus implements CommandBusInterface
{
    /** @var EventEnvelope[] */
    private array $eventsInProcess = [];

    public function __construct(
        private readonly CommandBusInterface $commandBus,
        private readonly EventStoreInterface $eventStore,
        private readonly EventPublisher $eventPublisher,
        private readonly EventProcessor $eventProcessor,
    ) {
    }

    public function tell(object $command): void
    {
        $commandException = null;
        try {
            $this->commandBus->tell($command);
        } catch (\Throwable $e) {
            $commandException = $e;
        } finally {
            $this->eventPublisher->__invoke();
            try{
                $this->processEvents();
            } finally {
                if ($commandException) {
                    throw $commandException;
                }
            }
        }
    }

    private function processEvents(): void
    {
        foreach ($this->eventStore->findEvents() as $event) {
            if ($this->isEventInProcess($event)) {
                continue;
            }
            $this->startProcessingEvent($event);
            $this->eventProcessor->__invoke($event);
            $this->finishProcessingEvent($event);
        }
    }

    private function isEventInProcess(EventEnvelope $envelope): bool
    {
        return array_key_exists($envelope->id->toString(), $this->eventsInProcess);
    }

    private function startProcessingEvent(EventEnvelope $envelope): void
    {
        $this->eventsInProcess[$envelope->id->toString()] = $envelope->event::class;
    }

    private function finishProcessingEvent(EventEnvelope $envelope): void
    {
        unset($this->eventsInProcess[$envelope->id->toString()]);
    }
}
