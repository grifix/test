<?php

declare(strict_types=1);

namespace Grifix\Test\EventStore;

use Grifix\EventStore\MessageBroker\MessageBrokerInterface;

final class MessageBrokerFake implements MessageBrokerInterface
{

    public function send(object $message): void
    {
    }

    public function startConsumer(callable $consumer): void
    {
    }
}
