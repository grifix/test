<?php

declare(strict_types=1);

namespace Grifix\Test;

use Grifix\Test\Fixture\FixtureFactory;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class AbstractIntegrationTest extends KernelTestCase
{
    protected readonly FixtureFactory $fixtureFactory;

    protected function setUp(): void
    {
        self::bootKernel();
        /** @var FixtureFactory $fixtureFactory */
        $fixtureFactory = self::$kernel->getContainer()->get(FixtureFactory::class);
        $this->fixtureFactory = $fixtureFactory;
        /** @var DatabaseManager $databaseManager */
        $databaseManager = self::$kernel->getContainer()->get(DatabaseManager::class);

        if ($databaseManager->hasDump()) {
            $databaseManager->restoreFromDump();
            return;
        }
        $databaseManager->resetDatabase();
        $databaseManager->createDump();
    }

    /**
     * @template T
     *
     * @param class-string<T> $alias
     *
     * @return T
     * @throws \Exception
     */
    protected function getDependency(string $alias): mixed
    {
        /** @var mixed $result */
        $result = self::$kernel->getContainer()->get($alias);
        return $result;
    }

    /**
     * @template T
     *
     * @param class-string<T> $fixtureClass
     *
     * @return T
     * @throws \Exception
     */
    protected function createFixture(string $fixtureClass): mixed
    {
        return $this->fixtureFactory->createFixture($fixtureClass);
    }
}
