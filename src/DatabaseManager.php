<?php

declare(strict_types=1);

namespace Grifix\Test;

use Doctrine\DBAL\Connection;

final class DatabaseManager
{
    private readonly array $connectionParams;
    private string $dumpPath;

    public function __construct(
        private readonly string $rootDir,
        private readonly string $env,
        private readonly Connection $connection,
    ) {
        $this->dumpPath = $this->rootDir . '/var/test_dump.sql';
        $this->connectionParams = $connection->getParams();
    }

    public function setDumpPath(string $dumpPath): void
    {
        $this->dumpPath = $this->rootDir . DIRECTORY_SEPARATOR . $dumpPath;
    }

    public function truncateTables():void{
        $schemas = $this->connection->fetchAllAssociative("SELECT schema_name FROM information_schema.schemata WHERE schema_name NOT LIKE 'pg_%' AND schema_name != 'information_schema'");
        foreach ($schemas as $shema) {
            $tables = $this->connection->fetchAllAssociative("SELECT table_name FROM information_schema.tables WHERE table_schema = '" . $shema['schema_name'] . "'");
            foreach ($tables as $table) {
                $this->connection->executeStatement("TRUNCATE TABLE " . $shema['schema_name'] . "." . $table['table_name'] . " CASCADE");
            }
        }
    }

    public function resetDatabase(): void
    {
        $this->terminateAllConnections();
        $this->exec(
            $this->rootDir . '/bin/console doctrine:database:drop --env=' . $this->env . ' --no-interaction --if-exists --force',
        );
        $this->exec($this->rootDir . '/bin/console doctrine:database:create --env=' . $this->env . ' --no-interaction');
        $this->exec(
            $this->rootDir . '/bin/console doctrine:migrations:migrate --env=' . $this->env . ' --no-interaction',
        );
    }

    private function terminateAllConnections(): void
    {
        $currentPid = $this->connection->executeQuery("SELECT pg_backend_pid()")->fetchFirstColumn()[0];
        $pids = $this->connection->executeQuery(
            "SELECT pid FROM pg_stat_activity WHERE datname = '" . $this->connectionParams['dbname'] . "'",
        )->fetchFirstColumn();
        foreach ($pids as $pid) {
            if ($pid == $currentPid) {
                continue;
            }
            $this->connection->executeStatement("SELECT pg_terminate_backend(" . $pid . ")");
        }
        $this->connection->close();
    }

    public function createDump(): void
    {
        $this->exec(
            sprintf(
                'export PGPASSWORD="%s" && pg_dump -h %s -p %s -d %s -U %s -wc --if-exists > %s',
                $this->connectionParams['password'],
                $this->connectionParams['host'],
                $this->connectionParams['port'],
                $this->connectionParams['dbname'],
                $this->connectionParams['user'],
                $this->dumpPath,
            ),
        );
    }

    public function hasDump(): bool
    {
        return is_file($this->dumpPath);
    }

    public function restoreFromDump(): void
    {
        $this->exec(
            sprintf(
                'export PGPASSWORD="%s" && psql -h %s -p %s -d %s -U %s -w < %s',
                $this->connectionParams['password'],
                $this->connectionParams['host'],
                $this->connectionParams['port'],
                $this->connectionParams['dbname'],
                $this->connectionParams['user'],
                $this->dumpPath,
            ),
        );
    }

    private function exec(string $command): void
    {
        $output = [];
        $result = 0;
        exec($command, $output, $result);
        if (0 !== $result) {
            throw new \Exception(implode("\n", $output));
        }
    }
}
