<?php

declare(strict_types=1);

namespace Grifix\Test;

use PHPUnit\Framework\TestCase;

abstract class AbstractTestCase extends TestCase
{
    public static function assertEventsWerePublished(object $object, ... $expectedEvents): void
    {
        $actualEvents = EventCollector::getEvents($object);
        foreach ($expectedEvents as $i => $expectedEvent) {
            self::assertArrayHasKey($i, $actualEvents, $expectedEvent::class . ' was not published!');
            self::assertEquals($expectedEvent, $actualEvents[$i]);
        }
    }

    public function expectExceptionWithMessage(string $expectException, ?string $expectExceptionMessage = null): void
    {
        $this->expectException($expectException);
        if ($expectExceptionMessage) {
            $this->expectExceptionMessage($expectExceptionMessage);
        }
    }
}
