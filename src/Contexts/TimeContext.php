<?php

declare(strict_types=1);

namespace Grifix\Test\Contexts;

use Behat\Behat\Context\Context;
use Grifix\Clock\ClockInterface;
use Grifix\Clock\FrozenClock;
use Grifix\Date\DateTime\DateTime;
use Grifix\Jwt\TestHelper as JwtTestHelper;

final class TimeContext implements Context
{

    /**
     * @param FrozenClock $clock
     */
    public function __construct(private readonly ClockInterface $clock)
    {
    }

    /**
     * @Given the time is :time
     */
    public function theTimeIs(string $time): void
    {
        $date = DateTime::fromString($time);
        $this->clock->freezeDate($date);
        JwtTestHelper::freezeCurrentDate($date);
    }
}
