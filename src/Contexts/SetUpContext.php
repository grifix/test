<?php

declare(strict_types=1);

namespace Grifix\Test\Contexts;

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Grifix\Test\DatabaseManager;

final class SetUpContext implements Context
{

    public function __construct(
        private readonly DatabaseManager $databaseManager,
    ) {
    }

    /**
     * @BeforeScenario
     */
    public function beforeScenario(BeforeScenarioScope $scope): void
    {
        if($this->databaseManager->hasDump()){
            $this->databaseManager->restoreFromDump();
            return;
        }
        $this->databaseManager->resetDatabase();
        $this->databaseManager->createDump();
    }
}
