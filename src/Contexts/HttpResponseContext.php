<?php

declare(strict_types=1);

namespace Grifix\Test\Contexts;

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Grifix\Test\TestHttpClient;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

final class HttpResponseContext implements Context
{
    public function __construct(private readonly TestHttpClient $httpClient)
    {
    }


    /**
     * @Then there should be the follwing data
     */
    public function thereShouldBeAnErrorThatPropertyIsRequired(string $property): void
    {
        Assert::assertEquals(400, $this->httpClient->getLastResponseStatusCode());
        Assert::assertEquals(
            'This field is missing.',
            $this->httpClient->getLastResponseJson()->hasElement('errors.' . $property)
        );
    }


    /**
     * @Then /^there should be the following data received:$/
     */
    public function thereShouldBeTheFollowingDataReceived(string $expectedData): void
    {
        $expectedData = Yaml::parse($expectedData);
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
        Assert::assertEquals($expectedData, $this->httpClient->getLastResponseJson()->getWrapped());
    }

    /**
     * @Then /^the returned result should be:$/
     */
    public function theReturnedResultShouldBe(string $expectedResultYaml): void
    {
        /** @var mixed[] $expectedResult */
        $expectedResult = Yaml::parse($expectedResultYaml);
        Assert::assertEquals(Response::HTTP_OK, $this->httpClient->getLastResponseStatusCode());
        Assert::assertEquals($expectedResult, $this->httpClient->getLastResponseJson()->getWrapped());
    }

}
