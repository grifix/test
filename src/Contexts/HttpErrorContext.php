<?php

declare(strict_types=1);

namespace Grifix\Test\Contexts;

use Behat\Behat\Context\Context;
use Grifix\Test\TestHttpClient;
use PHPUnit\Framework\Assert;

final class HttpErrorContext implements Context
{
    public function __construct(private readonly TestHttpClient $httpClient)
    {
    }


    /**
     * @Then there should be an error that :property is required
     */
    public function thereShouldBeAnErrorThatPropertyIsRequired(string $property): void
    {
        Assert::assertEquals(400, $this->httpClient->getLastResponseStatusCode());
        Assert::assertEquals(
            'This field is missing.',
            $this->httpClient->getLastResponseJson()->hasElement('errors.' . $property)
        );
    }

    /**
     * @Then there should be an error that :message
     */
    public function thereShouldBeAnErrorThat(string $message): void
    {
        $expectedResponseCode = 400;
        if (str_contains($message, 'does not exist')) {
            $expectedResponseCode = 404;
        }
        if (str_contains($message, 'Unauthorized')) {
            $expectedResponseCode = 401;
        }
        if (str_contains($message, 'Forbidden')) {
            $expectedResponseCode = 403;
        }
        Assert::assertEquals($expectedResponseCode, $this->httpClient->getLastResponseStatusCode());
        Assert::assertStringMatchesFormat($message, $this->httpClient->getLastResponseJson()->getElement('error.message'));
    }

    /**
     * @Then there should be an error that input is empty
     */
    public function thereShouldBeAnErrorThatInputIsEmpty(): void
    {
        Assert::assertEquals(400, $this->httpClient->getLastResponseStatusCode());
        $json = $this->httpClient->getLastResponseJson()->getWrapped();
        Assert::assertEquals('Invalid input!', $json['error']['message']);
        Assert::assertEquals('Array value found, but an object is required', array_shift($json['errors']));
    }
}
