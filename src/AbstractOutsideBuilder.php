<?php

declare(strict_types=1);

namespace Grifix\Test;

abstract class AbstractOutsideBuilder
{

    public static function create(): static
    {
        return new static();
    }

    /**
     * @template T
     * @param class-string<T> $interface
     * @return T
     */
    protected function doBuild(string $interface)
    {
        $result = \Mockery::mock($interface);
        foreach (get_class_methods($interface) as $method) {
            if ($method === 'publishEvent') {
                continue;
            }
            $propertyName = $method . 'Result';
            if (!property_exists($this, $propertyName)) {
                continue;
            }
            $value = $this->$propertyName;
            if(is_array($value)){
                $result->allows($method)->byDefault()->andReturnValues($value);
            }
            else{
                $result->allows($method)->byDefault()->andReturn($value);
            }
        }
        EventCollector::injectTo($result);
        return $result;
    }
}
