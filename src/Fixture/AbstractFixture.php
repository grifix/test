<?php

declare(strict_types=1);

namespace Grifix\Test\Fixture;

use Grifix\Clock\ClockInterface;
use Grifix\Clock\FrozenClock;
use Grifix\Date\DateTime\DateTime;
use Grifix\Framework\Ui\Application\ApplicationInterface;
use Grifix\ObjectRegistry\ObjectRegistryInterface;
use PHPUnit\TextUI\RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractFixture
{
    private bool $isBuilt = false;

    private ?DateTime $createdAt = null;

    private DateTime|bool|null $clockTime = null;

    /**
     * @param FrozenClock $clock
     */
    public function __construct(
        protected readonly ApplicationInterface $application,
        protected readonly ObjectRegistryInterface $objectRegistry,
        protected readonly ContainerInterface $container,
        protected readonly FixtureFactory $fixtureFactory,
        private readonly ClockInterface $clock,
    ) {
        $this->init();
    }

    protected function init(): void
    {
    }

    abstract protected function create(): void;

    protected function afterCreate(): void
    {
    }

    public function withCreatedAt(DateTime $createdAt): self
    {
        $this->assertIsNotBuilt();
        $this->createdAt = $createdAt;
        return $this;
    }

    public function build(?string $alias = null): static
    {
        $this->assertIsNotBuilt();
        if ($this->createdAt !== null) {
            $this->freezeTime($this->createdAt);
        }
        $this->create();
        $this->isBuilt = true;
        $this->afterCreate();
        if (null !== $alias) {
            $this->objectRegistry->addObject($this, $alias);
        }
        if ($this->createdAt !== null) {
            $this->restoreTime();
        }
        return $this;
    }

    public function duplicate(): static
    {
        $result = clone $this;
        $result->isBuilt = false;

        return $result;
    }

    protected function assertIsBuilt(): void
    {
        if (!$this->isBuilt) {
            throw new \Exception(sprintf('Fixture [%s] is not built!', static::class));
        }
    }

    protected function assertIsNotBuilt(): void
    {
        if ($this->isBuilt) {
            throw new \Exception(sprintf('Fixture [%s] is already built!', static::class));
        }
    }

    protected function freezeTime(DateTime $time): void
    {
        $this->assertCanFreezeTime();
        $this->assertIsNotBuilt();
        if ($this->clock->isFrozen()) {
            $this->clockTime = $this->clock->getCurrentDate();
        }
        $this->clock->freezeDate($time);
    }

    private function restoreTime(): void
    {
        $this->assertCanFreezeTime();
        if ($this->clockTime === true) {
            $this->clock->unfreezeTime();
        }

        if ($this->clockTime instanceof DateTime) {
            $this->clock->freezeDate($this->clockTime);
        }
        $this->clockTime = null;
    }

    private function assertCanFreezeTime(): void
    {
        if (!($this->clock instanceof FrozenClock)) {
            throw new RuntimeException('Cannot freeze time!');
        }
    }
}
