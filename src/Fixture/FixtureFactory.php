<?php

declare(strict_types=1);

namespace Grifix\Test\Fixture;

use Exception;
use Grifix\Clock\ClockInterface;
use Grifix\Clock\FrozenClock;
use Grifix\Framework\Ui\Application\ApplicationInterface;
use Grifix\ObjectRegistry\ObjectRegistryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class FixtureFactory
{
    public function __construct(
        private readonly ApplicationInterface $application,
        private readonly ObjectRegistryInterface $objectRegistry,
        private readonly ContainerInterface $container,
        private readonly ClockInterface $clock,
    ) {
    }

    /**
     * @template T
     *
     * @param class-string<T> $class
     *
     * @return T
     * @throws Exception
     */
    public function createFixture(string $class): mixed
    {
        if (!is_subclass_of($class, AbstractFixture::class)) {
            throw new Exception(sprintf('Fixture [%s] must extend [%s]!', $class, AbstractFixture::class));
        }
        return new $class($this->application, $this->objectRegistry, $this->container, $this, $this->clock);
    }
}
