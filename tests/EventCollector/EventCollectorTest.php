<?php

declare(strict_types=1);

namespace Grifix\Test\Tests\EventCollector;

use Grifix\Test\EventCollector;
use Grifix\Test\Tests\EventCollector\Dummies\Aggregate;
use Grifix\Test\Tests\EventCollector\Dummies\AggregateCreatedEvent;
use Grifix\Test\Tests\EventCollector\Dummies\OutsideInterface;
use PHPUnit\Framework\TestCase;

final class EventCollectorTest extends TestCase
{
    public function testItWorks(): void
    {
        $outside = \Mockery::mock(OutsideInterface::class);
        EventCollector::injectTo($outside);
        $aggregate = new Aggregate($outside);
        self::assertCount(1, EventCollector::getEvents($aggregate));
        self::assertEquals(new AggregateCreatedEvent(), EventCollector::getEvents($aggregate)[0]);
        EventCollector::clearEvents($aggregate);
        self::assertCount(0, EventCollector::getEvents($aggregate));
    }
}
