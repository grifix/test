<?php

declare(strict_types=1);

namespace Grifix\Test\Tests\EventCollector\Dummies;

interface OutsideInterface
{
    public function publishEvent(object $event):void;
}
