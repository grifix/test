<?php

declare(strict_types=1);

namespace Grifix\Test\Tests\EventCollector\Dummies;

final class Aggregate
{
    public function __construct(private readonly OutsideInterface $outside)
    {
        $this->outside->publishEvent(new AggregateCreatedEvent());
    }
}
