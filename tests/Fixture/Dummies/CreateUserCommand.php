<?php

declare(strict_types=1);

namespace Grifix\Test\Tests\Fixture\Dummies;

use Grifix\Uuid\Uuid;

final class CreateUserCommand
{

    public function __construct(public readonly Uuid $id)
    {
    }
}
