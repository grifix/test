<?php

declare(strict_types=1);

namespace Grifix\Test\Tests\Fixture\Dummies;

use Grifix\Test\Fixture\AbstractFixture;
use Grifix\Uuid\Uuid;

final class UserFixture extends AbstractFixture
{
    private Uuid $id;

    protected function init(): void
    {
        $this->id = Uuid::createRandom();
    }

    public function setId(Uuid $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    protected function create(): void
    {
        $this->application->tell(new CreateUserCommand($this->id));
    }
}
