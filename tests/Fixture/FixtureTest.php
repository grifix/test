<?php

declare(strict_types=1);

namespace Grifix\Test\Tests\Fixture;

use Grifix\Clock\FrozenClock;
use Grifix\Framework\Ui\Application\ApplicationInterface;
use Grifix\ObjectRegistry\ObjectRegistryInterface;
use Grifix\Test\Fixture\FixtureFactory;
use Grifix\Test\Tests\Fixture\Dummies\CreateUserCommand;
use Grifix\Test\Tests\Fixture\Dummies\InvalidFixture;
use Grifix\Test\Tests\Fixture\Dummies\UserFixture;
use Grifix\Uuid\Uuid;
use Hamcrest\Util;
use Mockery;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class FixtureTest extends TestCase
{

    protected function setUp(): void
    {
        Util::registerGlobalFunctions();
    }

    public function testItWorks(): void
    {
        $fixtureFactory = new FixtureFactory(
            $this->createApplication(
                new CreateUserCommand(Uuid::createFromString('6c3a3e9a-b436-40ea-8f2c-5c5251da34dd')),
            ),
            $this->createObjectRegistry(UserFixture::class, 'user'),
            Mockery::mock(Container::class),
            new FrozenClock(),
        );

        $fixture = $fixtureFactory->createFixture(UserFixture::class)
            ->setId(Uuid::createFromString('6c3a3e9a-b436-40ea-8f2c-5c5251da34dd'))
            ->build('user');
        self::assertInstanceOf(UserFixture::class, $fixture);
    }

    private function createApplication(object $expectedCommand): ApplicationInterface
    {
        return Mockery::mock(ApplicationInterface::class)
            ->shouldReceive('tell')
            ->with(equalTo($expectedCommand))
            ->getMock();
    }

    private function createObjectRegistry(string $expectedFixtureClass, string $expectedAlias): ObjectRegistryInterface
    {
        return Mockery::mock(ObjectRegistryInterface::class)
            ->shouldReceive('addObject')
            ->andReturnUsing(function (object $fixture, string $alias) use ($expectedFixtureClass, $expectedAlias) {
                self::assertEquals($expectedAlias, $alias);
                self::assertEquals($expectedFixtureClass, $fixture::class);
            })
            ->getMock();
    }

    public function testItFailsToBuildTwice(): void
    {
        $fixture = $this->createFixtureFactory()->createFixture(UserFixture::class);
        $fixture->build();
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Fixture [Grifix\Test\Tests\Fixture\Dummies\UserFixture] is already built!');
        $fixture->build();
    }

    public function testItDuplicates(): void
    {
        $fixture = $this->createFixtureFactory()->createFixture(UserFixture::class);
        $fixture->setId(Uuid::createFromString('fcc9e46c-072d-474c-9442-d38666d21cd4'));
        $fixture->build();
        $duplicate = $fixture->duplicate();
        $duplicate->build();
        self::assertEquals(Uuid::createFromString('fcc9e46c-072d-474c-9442-d38666d21cd4'), $duplicate->getId());
    }

    private function createFixtureFactory(): FixtureFactory
    {
        return new FixtureFactory(
            Mockery::mock(ApplicationInterface::class)->shouldReceive('tell')->getMock(),
            Mockery::mock(ObjectRegistryInterface::class)->shouldReceive('addObject')->getMock(),
            Mockery::mock(ContainerInterface::class),
            new FrozenClock(),
        );
    }

    public function testItFailsToCreateFixtureWithoutInheritance(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage(
            'Fixture [Grifix\Test\Tests\Fixture\Dummies\InvalidFixture] must extend [Grifix\Test\Fixture\AbstractFixture]!'
        );
        $this->createFixtureFactory()->createFixture(InvalidFixture::class);
    }
}
