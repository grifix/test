<?php

declare(strict_types=1);

namespace Grifix\Test\Tests\OutsideBuilder\Dummies;

use Grifix\Test\AbstractOutsideBuilder;

final class TestOutsideBuilder extends AbstractOutsideBuilder
{
    protected bool $isActiveResult = true;

    protected string $getEmailResult = 'user@example.com';

    public function setGetEmailResult(string $getEmailResult): self
    {
        $this->getEmailResult = $getEmailResult;
        return $this;
    }

    public function setIsActiveResult(bool $isActiveResult): self
    {
        $this->isActiveResult = $isActiveResult;
        return $this;
    }

    public function build(): TestOutsideInterface
    {
        return $this->doBuild(TestOutsideInterface::class);
    }
}
