<?php

declare(strict_types=1);

namespace Grifix\Test\Tests\OutsideBuilder\Dummies;

interface TestOutsideInterface
{
    public function getEmail(string $objectId): string;

    public function isActive(string $objectId): bool;

    public function publishEvent(object $event): void;
}
