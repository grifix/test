<?php

declare(strict_types=1);

namespace Grifix\Test\Tests\OutsideBuilder;

use Grifix\Test\Tests\OutsideBuilder\Dummies\TestOutsideBuilder;
use Grifix\Test\Tests\OutsideBuilder\Dummies\TestOutsideInterface;
use PHPUnit\Framework\TestCase;

final class OutsideBuilderTest extends TestCase
{
    public function testItBuilds(): void
    {
        $builder = TestOutsideBuilder::create();

        $builder->setGetEmailResult('user@grifix.net');
        $builder->setIsActiveResult(false);

        $mock = $builder->build();

        self::assertInstanceOf(TestOutsideInterface::class, $mock);
        self::assertEquals('user@grifix.net', $mock->getEmail('1'));
        self::assertFalse($mock->isActive('1'));
    }
}
